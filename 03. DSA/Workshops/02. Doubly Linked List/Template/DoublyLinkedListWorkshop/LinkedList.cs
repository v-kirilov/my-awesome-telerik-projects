﻿using System;
using System.Collections;

namespace DoublyLinkedListWorkshop
{
    public class LinkedList<T> : IList<T>
    {
        private Node head;
        private Node tail;
<<<<<<< HEAD
=======
        private int count = 0;
>>>>>>> ede04841df7e4c159e249be38b9890e70a3f8f3f

        public T Head
        {
            get
            {
<<<<<<< HEAD
                throw new NotImplementedException();
=======
                if (count == 0)
                {
                    throw new InvalidOperationException("No nodes");
                }
                return this.head.Value;
>>>>>>> ede04841df7e4c159e249be38b9890e70a3f8f3f
            }
        }

        public T Tail
        {
            get
            {
<<<<<<< HEAD
                throw new NotImplementedException();
=======
                if (count == 0)
                {
                    throw new InvalidOperationException("No nodes");
                }
                return this.tail.Value;
>>>>>>> ede04841df7e4c159e249be38b9890e70a3f8f3f
            }
        }

        public int Count
        {
            get
            {
<<<<<<< HEAD
                throw new NotImplementedException();
            }
            private set
            {
                throw new NotImplementedException();
            }
=======
                return this.count;
                //throw new NotImplementedException();
            }
            //private set
            //{
            //    this.count = 0;
            //    //throw new NotImplementedException();
            //}
>>>>>>> ede04841df7e4c159e249be38b9890e70a3f8f3f
        }

        public void AddFirst(T value)
        {
<<<<<<< HEAD
            throw new NotImplementedException();
=======
            if (count == 0)
            {
                this.head = new Node(value);
                this.tail = new Node(value);
                count++;
            }
            else if (count == 1)
            {
                this.head = new Node(value);
                this.head.Next = this.tail;
                this.tail.Prev = this.head;
                count++;
            }
            else
            {
                Node oldHead = this.head;
                oldHead.Next = this.head.Next;

                Node newNode = new Node(value);
                this.head = newNode;
                this.head.Next = oldHead;

                oldHead.Prev = this.head;
                count++;
            }
>>>>>>> ede04841df7e4c159e249be38b9890e70a3f8f3f
        }

        public void AddLast(T value)
        {
<<<<<<< HEAD
            throw new NotImplementedException();
=======
            if (count == 0)
            {
                this.head = new Node(value);
                this.tail = new Node(value);
                count++;
            }
            else if (count == 1)
            {
                this.tail = new Node(value);
                this.head.Next = this.tail;
                this.tail.Prev = this.head;
                count++;
            }
            else
            {
                Node oldTail = this.tail;
                oldTail.Prev = this.tail.Prev;

                Node newNode = new Node(value);
                this.tail = newNode;
                this.tail.Prev = oldTail;

                oldTail.Next = this.tail;
                count++;
            }
>>>>>>> ede04841df7e4c159e249be38b9890e70a3f8f3f
        }

        public void Add(int index, T value)
        {
<<<<<<< HEAD
            throw new NotImplementedException();
=======
            if (index > count || index < 0)
            {
                throw new ArgumentOutOfRangeException("Index is out of range");
            }
            if (index == 0)
            {
                AddFirst(value);

            }
            else if (index == count)
            {
                AddLast(value);
            }
            else
            {
                //Node current = new Node(Get(index-1));
                //Node nodeToInsert = new Node(value);
                //current.Next = nodeToInsert;
                //current.Next.Prev = nodeToInsert;
                //count++;

                Node current = this.head;
                for (int i = 0; i < index - 1; i++)
                {
                    current = current.Next;
                }
                Node nodeToInsert = new Node(value);
                current.Next = nodeToInsert;
                current.Next.Prev = nodeToInsert;
                count++;
            }


>>>>>>> ede04841df7e4c159e249be38b9890e70a3f8f3f
        }

        public T Get(int index)
        {
<<<<<<< HEAD
            throw new NotImplementedException();
=======
            if (index > count || index < 0)
            {
                throw new ArgumentOutOfRangeException("Index is out of range");
            }
            var current = this.head;
            for (int i = 0; i < index; i++)
            {
                current = current.Next;
            }
            return current.Value;
>>>>>>> ede04841df7e4c159e249be38b9890e70a3f8f3f
        }

        public int IndexOf(T value)
        {
<<<<<<< HEAD
            throw new NotImplementedException();
=======
            if (count == 0)
            {
                return -1;
            }
            ListEnumerator list = new ListEnumerator(this.head);
            int counter = -1;
            while (list.MoveNext())
            {
                counter++;
                if (Equals(value, list.Current))
                {
                    return counter;
                }
            }
            return -1;

>>>>>>> ede04841df7e4c159e249be38b9890e70a3f8f3f
        }

        public T RemoveFirst()
        {
<<<<<<< HEAD
            throw new NotImplementedException();
=======
            if (count == 0)
            {
                throw new InvalidOperationException("No elements.");
            }
            else if (count == 1)
            {

                count--;
                Node oldHead = this.head;
                this.head = null;
                this.tail = null;
                return oldHead.Value;
            }

            Node currentHead = this.head;
            this.head = this.head.Next;
            this.head.Prev = null;
            count--;
            return currentHead.Value;


>>>>>>> ede04841df7e4c159e249be38b9890e70a3f8f3f
        }

        public T RemoveLast()
        {
<<<<<<< HEAD
            throw new NotImplementedException();
=======
            if (count == 0)
            {
                throw new InvalidOperationException("No elements");
            }
            if (count == 1)
            {
                Node current = this.head;
                this.tail = null;
                this.head = null;
                count--;
                return current.Value;
            }
            var oldTail = this.tail;
            this.tail = this.tail.Prev;
            this.tail.Next = null;
            count--;
            return oldTail.Value;
>>>>>>> ede04841df7e4c159e249be38b9890e70a3f8f3f
        }

        /// <summary>
        /// Enumerates over the linked list values from Head to Tail
        /// </summary>
        /// <returns>A Head to Tail enumerator</returns>
        System.Collections.Generic.IEnumerator<T> System.Collections.Generic.IEnumerable<T>.GetEnumerator()
        {
            return new ListEnumerator(this.head);
        }

        /// <summary>
        /// Enumerates over the linked list values from Head to Tail
        /// </summary>
        /// <returns>A Head to Tail enumerator</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((System.Collections.Generic.IEnumerable<T>)this).GetEnumerator();
        }

        // Use private nested class so that LinkedList users
        // don't know about the LinkedList internal structure
        private class Node
        {
            public Node(T value)
            {
                this.Value = value;
            }

            public T Value
            {
                get;
                private set;
            }

            public Node Next
            {
                get;
                set;
            }

            public Node Prev
            {
                get;
                set;
            }
        }

        // List enumerator that enables traversing all nodes of a list in a foreach loop
        private class ListEnumerator : System.Collections.Generic.IEnumerator<T>
        {
            private Node start;
            private Node current;

            internal ListEnumerator(Node head)
            {
                this.start = head;
                this.current = null;
            }

            public T Current
            {
                get
                {
                    if (this.current == null)
                    {
                        throw new InvalidOperationException();
                    }
                    return this.current.Value;
                }
            }

            object IEnumerator.Current
            {
                get
                {
                    return this.Current;
                }
            }

            public void Dispose()
            {
            }

            public bool MoveNext()
            {
                if (current == null)
                {
                    current = this.start;
                    return true;
                }

                if (current.Next == null)
                {
                    return false;
                }

                current = current.Next;
                return true;
            }

            public void Reset()
            {
                this.current = null;
            }
        }
    }
}