﻿using AspNetCoreDemo.Exceptions;
using AspNetCoreDemo.Helpers;
using AspNetCoreDemo.Models;
using AspNetCoreDemo.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCoreDemo.Controllers
{
    [ApiController]
    [Route("api/beers")]
    public class BeersApiController : ControllerBase
    {
        private readonly IBeersService beerService;
        private readonly ModelMapper modelMapper;
        private readonly AuthManager authManager;
        public BeersApiController(IBeersService beerService,ModelMapper modelMapper,AuthManager authManager)
        {

            this.beerService = beerService;
            this.modelMapper = modelMapper;
            this.authManager = authManager;
        }

        [HttpGet("")]
        public IActionResult GetBeers([FromQuery] BeerQueryParameters filterPar)
        {
            List<Beer> result = this.beerService.FilterBy(filterPar);
            return this.StatusCode(StatusCodes.Status200OK, result);
        }

        [HttpGet("{id}")]
        public IActionResult GetBeerById(int id)
        {
            try
            {
                Beer beer = this.beerService.GetById(id);
                return this.StatusCode(StatusCodes.Status200OK, beer);
            }
            catch (EntityNotFoundException e)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }

        [HttpPost("")]
        public IActionResult CreateBeer([FromHeader] string username,[FromBody] BeerDto beerDto)
        {
            try
            {
                User user = this.authManager.TryGetUser(username);
                Beer newBeer = this.modelMapper.Map(beerDto);
                Beer createBeer = this.beerService.Create(newBeer);
                return this.StatusCode(StatusCodes.Status200OK, createBeer);
            }catch(UnauthorizedOperationException e)
            {
                return this.StatusCode(StatusCodes.Status401Unauthorized, e.Message);
            }
            catch (DuplicateEntitiyException e)
            {
                return this.StatusCode(StatusCodes.Status409Conflict, e.Message);
            }

        }

        [HttpPut("{id}")]
        public IActionResult UpdateBeer(int id, [FromHeader] User user, [FromBody] BeerDto beerDto)
        {
            
            try
            {
                Beer beer = this.modelMapper.Map(beerDto);
                Beer beerToUpdate = this.beerService.Update(id, beer,user);
                return this.StatusCode(StatusCodes.Status200OK, beerToUpdate);

            }
            catch (EntityNotFoundException e)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
            }catch(DuplicateEntitiyException e)
            {
                return this.StatusCode(StatusCodes.Status409Conflict, e.Message);

            }
        }

        
        [HttpDelete("{id}")]
        public IActionResult DeleteBeer(int id)
        {
            try
            {
                this.beerService.Delete(id);
                return this.StatusCode(StatusCodes.Status200OK);
            }
            catch (EntityNotFoundException e)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }
    }
}
