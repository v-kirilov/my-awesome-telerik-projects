﻿using AspNetCoreDemo.Exceptions;
using AspNetCoreDemo.Models;
using AspNetCoreDemo.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;


namespace AspNetCoreDemo.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UsersApiController : ControllerBase
    {
        private readonly IUserService userService;

        public UsersApiController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet("")]
        public IActionResult GetUsers()
        {
            return this.StatusCode(StatusCodes.Status200OK, this.userService.GetAll());
        }

        [HttpGet("{id}")]
        public IActionResult GetUserById(int id)
        {
            try
            {
                User user = this.userService.GetById(id);
                return this.StatusCode(StatusCodes.Status200OK, user);
            }
            catch (EntityNotFoundException e)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }
        //[HttpGet("{name}")]
        //public IActionResult GetByName(string name)
        //{
        //    try
        //    {
        //        User existingUser = this.userService.GetByName(name);
        //        return this.StatusCode(StatusCodes.Status200OK, existingUser);

        //    }
        //    catch (EntityNotFoundException e)
        //    {
        //        return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
        //    }
        //}
        //[HttpGet("{email}")]
        //public IActionResult GetByEmail(string email)
        //{
        //    try
        //    {
        //        User existingUser = this.userService.GetByEmail(email);
        //        return this.StatusCode(StatusCodes.Status200OK, existingUser);

        //    }
        //    catch (EntityNotFoundException e)
        //    {
        //        return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
        //    }
        //}
        [HttpPost("")]
        public IActionResult CreateUser([FromBody] User user)
        {
            try
            {
                User newUser = this.userService.CreateUser(user);
                return this.StatusCode(StatusCodes.Status200OK, newUser);
            }
            catch (DuplicateEntitiyException e)
            {
                return this.StatusCode(StatusCodes.Status409Conflict, e.Message);
                
            }
        }

        [HttpPut("{Id}")]
        public IActionResult UpdateUser(int id , [FromBody] User user)
        {
            if (id != user.Id)
            {
                return this.StatusCode(StatusCodes.Status409Conflict, "Id mismatch!");
            }

            try
            {
                User userToUpdate = this.userService.UpdateUser(id, user);
                return this.StatusCode(StatusCodes.Status200OK, userToUpdate);
            }
            catch (DuplicateEntitiyException e)
            {
                return this.StatusCode(StatusCodes.Status409Conflict, e.Message);
            }catch(EntityNotFoundException e)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }

        [HttpDelete("{Id}")]
        public IActionResult DeleteUser(int id)
        {
            try
            {
                this.userService.DeleteUser(id);
                return this.StatusCode(StatusCodes.Status200OK);
            }
            catch (EntityNotFoundException e)
            {
                return this.StatusCode(StatusCodes.Status404NotFound, e.Message);
            }
        }
    }
}
