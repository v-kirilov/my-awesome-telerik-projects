﻿using AspNetCoreDemo.Exceptions;
using AspNetCoreDemo.Services;
using AspNetCoreDemo.Models;


namespace AspNetCoreDemo.Helpers
{
    public class AuthManager
    {
        private readonly IUserService userService;
        public AuthManager(IUserService userService)
        {
            this.userService = userService;
        }

        public User TryGetUser(string username)
        {
            try
            {
                return this.userService.GetByUserName(username);
            }
            catch (EntityNotFoundException)
            {
                throw new UnauthorizedOperationException("Not Found");
            }
        }
    }
}
