﻿using AspNetCoreDemo.Models;
using AspNetCoreDemo.Services;

namespace AspNetCoreDemo.Helpers
{
    public class ModelMapper
    {
        private readonly IStylesService styleService;

        public ModelMapper(IStylesService styleService)
        {
            this.styleService = styleService;
        }
        public Beer Map(BeerDto dto)
        {
            return new Beer
            {
                Name = dto.Name,
                Abv = dto.Abv,
                Style = styleService.GetById(dto.StyleId)
            };
        }

    }
}

