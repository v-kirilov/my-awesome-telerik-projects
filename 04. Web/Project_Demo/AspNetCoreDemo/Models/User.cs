﻿using System.ComponentModel.DataAnnotations;

namespace AspNetCoreDemo.Models
{
    public class User
    {
        [Range(0, int.MaxValue, ErrorMessage = "Please enter a valid integer Number")]
        public int Id { get; set; }

        [StringLength(25, MinimumLength = 2, ErrorMessage = "The {0} must be between {1} and {2} characters long.")]

        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
    }
}
