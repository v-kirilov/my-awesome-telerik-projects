﻿using AspNetCoreDemo.Exceptions;
using AspNetCoreDemo.Models;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCoreDemo.Repositories
{
    public class BeersRepository : IBeersRepository
    {
        private readonly List<Beer> beers;
        private readonly IStylesRepository stylesRepository;
        private readonly IUserRepository userRepo;

        public BeersRepository(IStylesRepository stylesRepository, IUserRepository userRepo)
        {
            this.stylesRepository = stylesRepository;
            this.userRepo = userRepo;
            this.beers = new List<Beer>()
            {
                new Beer
                {
                    Id = 1,
                    Name = "Glarus English Ale",
                    Abv = 4.6,
                    Style = this.stylesRepository.GetById(1),
                    CreatedBy = this.userRepo.GetById(2) //Georgi
                },
                new Beer
                {
                    Id = 2,
                    Name = "Rhombus Porter",
                    Abv = 5.0,
                    Style = this.stylesRepository.GetById(2),
                    CreatedBy = this.userRepo.GetById(2) //Georgi
                },
                new Beer
                {
                    Id = 3,
                    Name = "Opasen Char",
                    Abv = 6.6,
                    Style = this.stylesRepository.GetById(3),
                    CreatedBy = this.userRepo.GetById(3) //Georgi
                }
            };
        }

        public List<Beer> GetAll()
        {
            return this.beers;
        }

        public List<Beer> FilterBy(BeerQueryParameters filterPar)
        {
            List<Beer> result = this.beers;
            if (!string.IsNullOrEmpty(filterPar.Name))
            {
                result = result.FindAll(x => x.Name.Contains(filterPar.Name));
            }
            if (filterPar.MinAbv.HasValue)
            {
                result = result.FindAll(x => x.Abv >= filterPar.MinAbv);
            }
            if (filterPar.MaxAbv.HasValue)
            {
                result = result.FindAll(x => x.Abv <= filterPar.MaxAbv);
            }

            if (filterPar.StyleId.HasValue)
            {
                result = result.FindAll(x => x.Style.Id == filterPar.StyleId);
            }
            if (!string.IsNullOrEmpty(filterPar.StyleName))
            {
                result = result.FindAll(x => x.Style.Name.Contains(filterPar.StyleName));
            }

            if (!string.IsNullOrEmpty(filterPar.SortBy))
            {
                if (filterPar.SortBy.Equals("name", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    result = result.OrderBy(x => x.Name).ToList();
                }
                if (filterPar.SortBy.Equals("abv", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    result = result.OrderBy(x => x.Abv).ToList();
                }
                if (!string.IsNullOrEmpty(filterPar.SortOrder) && filterPar.SortOrder.Equals("desc", System.StringComparison.InvariantCultureIgnoreCase))
                {
                    result.Reverse();
                }
            }

            return result;
        }

        public Beer GetById(int id)
        {
            Beer beer = this.beers.FirstOrDefault(x => x.Id == id);
            return beer ?? throw new EntityNotFoundException();
        }
        public Beer CreateBeer(Beer beer)
        {
            this.beers.Add(beer);
            beer.Id = this.beers.Count;
            return beer;
        }


        public Beer GetByName(string name)
        {
            Beer beer = this.beers.FirstOrDefault(x => x.Name == name);
            return beer ?? throw new EntityNotFoundException();
        }

        public Beer UpdateBeer(int id, Beer beer)
        {
            Beer beerToUpdate = this.GetById(id);
            beerToUpdate.Abv = beer.Abv;
            beerToUpdate.Style = beer.Style;
            return beerToUpdate;
        }
        public void Delete(int id)
        {
            Beer beerToDelete = this.GetById(id);
            this.beers.Remove(beerToDelete);
        }
    }
}
