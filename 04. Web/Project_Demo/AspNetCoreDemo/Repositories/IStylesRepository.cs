﻿using AspNetCoreDemo.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Repositories
{
    public interface IStylesRepository
    {
        List<Style> GetAll();
        Style GetById(int id);
    }
}
