﻿using AspNetCoreDemo.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Repositories
{
    public interface IUserRepository
    {
        List<User> GetAllUsers();
        User GetById(int id);
        User GetByUserName(string name);
        User GetByFirstName(string name);
        User GetByLastName(string name);
        User GetByEmail(string email);
        User CreateUser(User user);
        User UpdateUser(int id,User user);
        void DeleteUser(int id);
    }
}
