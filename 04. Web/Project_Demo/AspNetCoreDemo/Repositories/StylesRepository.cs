﻿using AspNetCoreDemo.Exceptions;
using AspNetCoreDemo.Models;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCoreDemo.Repositories
{
    public class StylesRepository : IStylesRepository
    {
        private readonly List<Style> styles;

        public StylesRepository()
        {
            this.styles = new List<Style>
            {
                new Style {Id=1 , Name="Special Ale"},
                new Style {Id =2, Name = "English Porter"},
                new Style { Id=3, Name= "Indian Pale Ale"}
            };
        }
        public List<Style> GetAll()
        {
            return this.styles;
        }

        public Style GetById(int id)
        {
            Style style = this.styles.FirstOrDefault(x => x.Id == id);
            return style?? throw new EntityNotFoundException();
        }
    }
}
