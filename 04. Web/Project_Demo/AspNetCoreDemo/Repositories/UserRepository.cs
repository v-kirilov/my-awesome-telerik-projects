﻿using AspNetCoreDemo.Exceptions;
using AspNetCoreDemo.Models;
using System.Collections.Generic;
using System.Linq;

namespace AspNetCoreDemo.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly List<User> users;
        public UserRepository()
        {
            this.users = new List<User>()
            {
                new User()
                {
                    Id = 1,
                    UserName = "admin",
                    IsAdmin = true,
                },
                new User()
                {
                    Id = 2,
                    UserName = "Georgi",
                    IsAdmin = false,
                },
                 new User()
                {
                    Id = 3,
                    UserName = "Peter",
                    IsAdmin = false,
                },
            };
        }
        public List<User> GetAllUsers()
        {
            return this.users;
        }

        public User GetById(int id)
        {
            User userToGet = this.users.FirstOrDefault(u => u.Id == id);
            return userToGet ?? throw new EntityNotFoundException();
        }

        public User CreateUser(User user)
        {
            this.users.Add(user);
            user.Id = this.users.Count;
            return user;
        }

        public User GetByUserName(string name)
        {
            User userToGet = this.users.Where(u => u.UserName == name).FirstOrDefault();
            return userToGet ?? throw new EntityNotFoundException();
        }

        public User UpdateUser(int id, User user)
        {
            User userToUpdate = GetById(id);
            userToUpdate.FirstName = user.FirstName;
            userToUpdate.LastName = user.LastName;
            userToUpdate.Email = user.Email;
            return userToUpdate;
        }

        public void DeleteUser(int id)
        {
            User userToDelete = GetById(id);
            this.users.Remove(userToDelete);
        }

        public User GetByEmail(string email)
        {
            User selectUserByMail = this.users.FirstOrDefault(u => u.Email == email);
            return selectUserByMail ?? throw new EntityNotFoundException();
        }

        public User GetByFirstName(string firstName)
        {
            return this.users.FirstOrDefault(u => u.FirstName == firstName);
        }

        public User GetByLastName(string lastName)
        {
            return this.users.FirstOrDefault(u => u.LastName == lastName);

        }
    }
}
