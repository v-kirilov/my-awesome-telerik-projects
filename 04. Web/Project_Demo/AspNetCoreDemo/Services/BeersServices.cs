﻿using AspNetCoreDemo.Exceptions;
using AspNetCoreDemo.Models;
using AspNetCoreDemo.Repositories;
using System.Collections.Generic;

namespace AspNetCoreDemo.Services
{
    public class BeersServices : IBeersService
    {
        private const string ModifyBeerErrorMessage = "Only owner or admin can modify a beer.";

        private readonly IBeersRepository repository;


        public BeersServices(IBeersRepository repository)
        {
            this.repository = repository;
        }
        public List<Beer> GetAll()
        {
            return this.repository.GetAll();
        }
        public List<Beer> FilterBy(BeerQueryParameters filterParam)
        {
            return this.repository.FilterBy(filterParam);
        }
        public Beer Create(Beer beer)
        {
            bool isThereDuplicate = true;
            try
            {
                this.repository.GetByName(beer.Name);
            }
            catch (EntityNotFoundException)
            {
               isThereDuplicate = false;
            }
            if (isThereDuplicate)
            {
                throw new DuplicateEntitiyException();
            }

            Beer createBeer = this.repository.CreateBeer(beer);
            return createBeer;
        }

        public void Delete(int id)
        {
            this.repository.Delete(id);
        }


        public Beer GetById(int id)
        {
            return this.repository.GetById(id);
        }

        public Beer Update(int id, Beer beer,User user)
        {
            Beer beetToUpdate = this.repository.GetById(id);
            if (!beetToUpdate.CreatedBy.Equals(user)&&!user.IsAdmin)
            {
                throw new UnauthorizedOperationException(ModifyBeerErrorMessage);

            }


            bool IsThereDuplicate = true;

            try
            {
                Beer existingBeer = this.repository.GetByName(beer.Name);
                if (existingBeer.Id == id)
                {
                    IsThereDuplicate = false;
                }
            }
            catch (EntityNotFoundException)
            {
                IsThereDuplicate = false;
            }

            if (IsThereDuplicate)
            {
                throw new DuplicateEntitiyException();
            }
            Beer updateBeer = this.repository.UpdateBeer(id, beer);
            return updateBeer;
        }

        public Beer GetByName(string name)
        {
            return this.repository.GetByName(name);
        }
    }
}
