﻿using AspNetCoreDemo.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Services
{
    public interface IBeersService
    {
        List<Beer> GetAll();
        Beer GetById(int id);
        Beer GetByName(string name);
        Beer Create(Beer beer);
        Beer Update(int id, Beer beer,User user);
        void Delete(int id);

        List<Beer> FilterBy(BeerQueryParameters filterPar);
    }
}
