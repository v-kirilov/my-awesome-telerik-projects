﻿using AspNetCoreDemo.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Services
{
    public interface IStylesService
    {
        List<Style> GetAll();
        Style GetById(int id);
    }
}
