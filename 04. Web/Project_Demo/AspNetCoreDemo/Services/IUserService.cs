﻿using AspNetCoreDemo.Models;
using System.Collections.Generic;

namespace AspNetCoreDemo.Services
{
    public interface IUserService
    {
        List<User> GetAll();
        User GetById(int id);
        User GetByUserName(string userName);
        User GetByFirstName(string firstName);
        User GetByLastName(string lastName);
        User GetByEmail(string email);
        User CreateUser(User user);
        User UpdateUser(int id, User user);
        void DeleteUser(int id);
    }
}
