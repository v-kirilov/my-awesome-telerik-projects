﻿using AspNetCoreDemo.Models;
using AspNetCoreDemo.Repositories;
using System.Collections.Generic;

namespace AspNetCoreDemo.Services
{
    public class StylesService : IStylesService
    {
        private readonly IStylesRepository stylesrepository;
        public StylesService(IStylesRepository stylesrepository)
        {
            this.stylesrepository = stylesrepository;
        }

        public List<Style> GetAll()
        {
            return this.stylesrepository.GetAll();
        }

        public Style GetById(int id)
        {
            return this.stylesrepository.GetById(id);
        }

    }
}
