﻿using AspNetCoreDemo.Models;
using AspNetCoreDemo.Exceptions;
using AspNetCoreDemo.Repositories;
using System.Collections.Generic;

namespace AspNetCoreDemo.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository repository;
        public UserService(IUserRepository repository)
        {
            this.repository = repository;
        }
        public List<User> GetAll()
        {
            return this.repository.GetAllUsers();
        }
        public User CreateUser(User user)
        {
            bool isThereDuplicate = true;
            try
            {
                this.repository.GetByUserName(user.UserName);
            }
            catch (EntityNotFoundException)
            {
                 isThereDuplicate = false;
            }
            if (isThereDuplicate)
            {
                throw new DuplicateEntitiyException();
            }
            User newUser = this.repository.CreateUser(user);
            return newUser;
        }

        public void DeleteUser(int id)
        {
            this.repository.DeleteUser(id);
        }

        public User GetById(int id)
        {
            return this.repository.GetById(id);
        }

        public User GetByEmail(string email)
        {
            return this.repository.GetByEmail(email);
        }

        public User GetByUserName(string name)
        {
            return this.repository.GetByUserName(name);
        }

        public User UpdateUser(int id, User user)
        {
            bool isThereDuplicate = true;
            try
            {
                User existingUser = this.repository.GetById(id);
                if (existingUser.Id == id)
                {
                    isThereDuplicate = false;
                }
            }
            catch (EntityNotFoundException)
            {
                isThereDuplicate = false;
            }
            if (isThereDuplicate)
            {
                throw new DuplicateEntitiyException();
            }

            User updateUser = this.repository.UpdateUser(id,user);
            return updateUser;
        }

        public User GetByFirstName(string firstName)
        {
            return this.repository.GetByFirstName(firstName);
        }

        public User GetByLastName(string lastName)
        {
            return this.repository.GetByLastName(lastName);
        }
    }
}
