using AspNetCoreDemo.Helpers;
using AspNetCoreDemo.Repositories;
using AspNetCoreDemo.Services;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace AspNetCoreDemo
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            //Repos
            services.AddSingleton<IBeersRepository, BeersRepository>();
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddScoped<IStylesRepository, StylesRepository>();
            //Services
            services.AddScoped<IBeersService, BeersServices>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IStylesService, StylesService>();

            services.AddScoped<ModelMapper>();
            services.AddScoped<AuthManager>();
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseDeveloperExceptionPage();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
