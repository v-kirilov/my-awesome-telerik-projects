﻿using System;
using System.Collections.Generic;
using System.Text;
using ChessPieces.Pieces;

namespace ChessPieces
{
    public static class Board
    {
        private static List<ChessPiece> whitePieces = new List<ChessPiece>();
        private static List<ChessPiece> blackPieces = new List<ChessPiece>();

        public static void AddWhitepiece(ChessPiece piece)
        {
            if (piece.Name == "King")
            {
                throw new ArgumentException("You already have a White King on the Board");
            }
            else
            {

                whitePieces.Add(piece);
            }
        }
        public static void AddBlackPiece(ChessPiece piece)
        {
            if (piece.Name == "King")
            {
                throw new ArgumentException("You already have a Black King on the Board");
            }
            else
            {
                blackPieces.Add(piece);
            }
        }

        public static void PiecesInfo()
        {
            if (whitePieces.Count == 0)
            {
                Console.WriteLine();
                Console.WriteLine("The Board has no white peaces present at the moment");
            }
            else
            {
                foreach (var piece in whitePieces)
                {
                    Console.WriteLine(piece.ViewInfo());

                }

            }
            if (blackPieces.Count == 0)
            {
                Console.WriteLine();
                Console.WriteLine("The Board has no black peaces present at the moment");
            }
            else
            {
                foreach (var piece in blackPieces)
                {
                    Console.WriteLine(piece.ViewInfo());

                }

            }


        }
    }
}

