﻿using System;
using System.Collections.Generic;
using System.Text;
using ChessPieces.Pieces.Enums;

namespace ChessPieces.Pieces
{
    public abstract class ChessPiece
    {
        private string name;
        private Color color;

        public string Name
        {
            get
            {
                return name;
            }
            private set
            {
                name = value;
            }
        }

        public Color Color
        {
            get
            {
                return color;
            }
            private set
            {

                if (value != Color.white && value != Color.black)
                {
                    throw new ArgumentException("Color must be black or white.");
                }
                color = value;
            }
        }

        

        public ChessPiece(string name, Color color)
        {
            this.Name = name;
            this.Color = color;
        }

        public abstract void DrawMove(ChessPiece piece);

        public abstract string ViewInfo();
       


    }
}
