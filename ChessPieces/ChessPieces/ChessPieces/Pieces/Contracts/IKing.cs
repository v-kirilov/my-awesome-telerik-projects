﻿using ChessPieces.Pieces.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChessPieces.Pieces.Contracts
{
    internal interface IKing 
    {
        public Gender Gender { get; }

    }
}
