﻿using ChessPieces.Pieces.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChessPieces.Pieces.Contracts
{
    internal interface IKnight 
    {
        public Gender Gender { get; }

    }
}
