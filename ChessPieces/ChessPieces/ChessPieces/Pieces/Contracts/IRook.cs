﻿using ChessPieces.Pieces.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChessPieces.Pieces.Contracts
{
    internal interface IRook 
    {
        public Gender Gender { get; }

    }
}
