﻿using ChessPieces.Pieces.Contracts;
using ChessPieces.Pieces.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChessPieces.Pieces
{
    public class Pawn : ChessPiece, IPawn

    {
        public Pawn(string name, Color color, Gender gender)
            : base(name, color)
        {
            this.Gender = gender;
        }

        private Gender gender;
        public Gender Gender
        {
            get
            {
                return this.gender;
            }
            set
            {
                if (value != Gender.male && value != Gender.female)
                {
                    throw new ArgumentException("Gender must be valid.");
                }
                this.gender = value;
            }
        }

        public override void DrawMove(ChessPiece piece)
        {
            Console.WriteLine("=====");
            Console.WriteLine("A pawn moves like this:");
            Console.WriteLine("    #");
            Console.WriteLine("#   #");
            Console.WriteLine("=====");
        }

        public override string ViewInfo()
        {
            return $"The pawn can move only forward one square at a time.";
        }
    }
}
