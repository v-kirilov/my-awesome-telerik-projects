﻿using System;
using ChessPieces.Pieces;
using ChessPieces.Pieces.Enums;

namespace ChessPieces
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ChessPiece pawn1 = new Pawn("Pesho1", Color.white, Gender.female);
            pawn1.DrawMove(pawn1);

            Board.AddWhitepiece(pawn1);
            Board.PiecesInfo();

        }
    }
}
