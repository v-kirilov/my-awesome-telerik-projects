﻿using System;
using System.Numerics;
using System.Diagnostics;

namespace P04_Zig_Zag
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string[] input = Console.ReadLine().Split(' ');

            int n = int.Parse(input[0]);
            int m = int.Parse(input[1]);

            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();


            long[,] matrix = new long[n, m];

            matrix[0, 0] = 1;
            for (int i = 0; i < 1; i++)
            {
                for (int j = 1; j < m; j++)
                {
                    matrix[i, j] = matrix[i, j - 1] + 3;
                }
            }

            for (int i = 1; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    matrix[i, j] = matrix[i - 1, j] + 3;
                }
            }

            stopWatch.Stop();
            TimeSpan timeTaken = stopWatch.Elapsed;
            Console.WriteLine(timeTaken);

            stopWatch.Restart();

            BigInteger sum = 1;
            BigInteger currentSum = 0;

            int nextRow = 0;
            int nextCol = 0;

            for (int i = 0; i < matrix.GetLength(0) - 1; i++)
            {
                currentSum = 0;

                while (nextCol < matrix.GetLength(1))
                {

                    if (nextRow + 1 < matrix.GetLength(0) && nextCol + 1 < matrix.GetLength(1))
                    {
                        nextRow++;
                        nextCol++;
                        currentSum += matrix[nextRow, nextCol];

                    }
                    else
                    {
                        break;
                    }
                    if (nextCol + 1 < matrix.GetLength(1))
                    {
                        nextCol++;
                        nextRow--;
                        currentSum += matrix[nextRow, nextCol];
                    }
                    else
                    {
                        break;
                    }
                }
                if (nextRow + 1 >= matrix.GetLength(0))
                {
                    sum += currentSum;
                    break;
                }
                else
                {
                    sum += currentSum * 2;
                    nextRow += 1;
                    nextCol = 0;
                }

            }

            stopWatch.Stop();
            timeTaken = stopWatch.Elapsed;
            Console.WriteLine(timeTaken);
            Console.WriteLine(sum);
        }
    }
}
