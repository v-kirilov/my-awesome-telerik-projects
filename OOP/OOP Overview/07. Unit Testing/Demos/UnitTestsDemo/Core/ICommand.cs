﻿namespace UnitTestsDemo.Core
{
    public interface ICommand
    {
        void Execute();
    }
}
