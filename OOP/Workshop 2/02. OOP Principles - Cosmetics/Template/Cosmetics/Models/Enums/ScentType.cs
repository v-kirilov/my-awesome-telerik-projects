﻿namespace Cosmetics.Models.Enums
{
    public enum ScentType
    {
        lavender,
        vanilla,
        rose
    }
}
